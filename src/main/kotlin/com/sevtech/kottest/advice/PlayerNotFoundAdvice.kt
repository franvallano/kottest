package com.sevtech.kottest.advice

import com.sevtech.kottest.model.exception.PlayerNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class PlayerNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(PlayerNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun playerNotFoundHandler(advice: PlayerNotFoundException): String {
        return advice.message.toString()
    }

}