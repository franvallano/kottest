package com.sevtech.kottest.model

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "player")
data class Player (
        @Id val id: String?,
        val name:String = "",
        var totalScore: Int = 0,
        val history:List<ScoreEvent> = listOf()
){

    constructor(name:String) : this(null, name, 0, listOf())
    infix fun plusScore(score:Int) = Player( id, name, totalScore + score, history + ScoreEvent(score))

}
