package com.sevtech.kottest.model.exception

import java.lang.RuntimeException

class PlayerNotFoundException(id: Long?) : RuntimeException("Could not find player")