package com.sevtech.kottest.model.dto

import com.fasterxml.jackson.annotation.JsonInclude

@JsonInclude(JsonInclude.Include.NON_NULL)
data class UpdatePlayerDto (var name:String,
                            var score:Int)