package com.sevtech.kottest.model

import java.text.SimpleDateFormat
import java.util.*

data class ScoreEvent(val time: String = "",
                      val points:Int = 0){
    constructor(points: Int) : this(dateFormat.format(Date()), points)

    //Implementacion estaticos
    companion object {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    }
}