package com.sevtech.kottest.repository

import com.sevtech.kottest.model.Player
import org.springframework.data.mongodb.repository.MongoRepository

interface PlayerRepository : MongoRepository<Player, String> {
    fun findTop3ByOrderByTotalScoreDesc(): List<Player>
    fun findPlayerByName(name:String): Player
}