package com.sevtech.kottest.service

import com.sevtech.kottest.model.Player
import com.sevtech.kottest.model.dto.CreatePlayerDto
import com.sevtech.kottest.model.dto.UpdatePlayerDto

interface PlayerService {

    fun getPlayerById(id: String): Player
    fun getTop3Player(): List<Player>
    fun createPlayer(player: CreatePlayerDto): Player
    fun findAll(): List<Player>
    fun deletePlayer(id: String)
    fun addScore(player: UpdatePlayerDto): Int
    fun getPlayerByName(name: String): Player
}