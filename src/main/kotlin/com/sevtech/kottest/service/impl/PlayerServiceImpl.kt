package com.sevtech.kottest.service.impl

import com.sevtech.kottest.model.Player
import com.sevtech.kottest.model.dto.CreatePlayerDto
import com.sevtech.kottest.model.dto.UpdatePlayerDto
import com.sevtech.kottest.repository.PlayerRepository
import com.sevtech.kottest.service.PlayerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class PlayerServiceImpl : PlayerService {

    @Autowired
    lateinit var playerRepository: PlayerRepository

    override fun getPlayerById(id: String): Player {
         return playerRepository.findById(id).get()
    }

    override fun getPlayerByName(name: String): Player {
        return playerRepository.findPlayerByName(name)
    }

    override fun getTop3Player(): List<Player> {
        return playerRepository.findTop3ByOrderByTotalScoreDesc()
    }

    override fun createPlayer(player: CreatePlayerDto): Player {
        return playerRepository.save(Player(player.name))
    }

    override fun deletePlayer(id: String) {
        playerRepository.deleteById(id)
    }

    override fun findAll(): List<Player> {
        return playerRepository.findAll().toList()
    }

    override fun addScore(player: UpdatePlayerDto) : Int {
        val pl = playerRepository.findById(player.name).get()
        pl.plusScore(player.score)
        return playerRepository.save(pl).totalScore
    }

}