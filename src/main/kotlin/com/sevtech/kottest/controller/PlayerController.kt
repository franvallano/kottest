package com.sevtech.kottest.controller

import com.sevtech.kottest.model.Player
import com.sevtech.kottest.model.dto.CreatePlayerDto
import com.sevtech.kottest.model.dto.UpdatePlayerDto
import com.sevtech.kottest.service.PlayerService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/player")
class PlayerController {

    private val logger = LoggerFactory.getLogger(PlayerController::class.java)

    @Autowired
    lateinit var playerService: PlayerService

    @GetMapping("/{id}")
    fun getPlayer(@PathVariable id: String): ResponseEntity<Player> {
        logger.info("getPlayer: id - $id")
        return ResponseEntity.ok().body(playerService.getPlayerById(id))
    }

    @GetMapping("/name/{name}")
    fun getPlayerByName(@PathVariable name: String): Player {
        logger.info("getPlayerByName: name - $name")
        return playerService.getPlayerByName(name)
    }

    @GetMapping("/top")
    fun getTopPlayer3() = playerService.getTop3Player()

    @GetMapping("/all")
    fun getAll() = playerService.findAll()

    @PostMapping("/create")
    fun createPlayer(@RequestBody player: CreatePlayerDto): Player {
        logger.info("createPlayer: player - $player")
        return playerService.createPlayer(player)
    }

    @PutMapping("/update")
    fun updateScore(@RequestBody player: UpdatePlayerDto): Int{
        logger.info("updateScore: player - $player")
        return playerService.addScore(player)
    }

    @DeleteMapping("/delete")
    fun deletePlayer(@RequestBody id: String) = playerService.deletePlayer(id)

}
