# Kotlin + MongoDB + Spring Boot

* Build image as follow:

```bash
~$ docker run -d --name kottest-player-1 franvallano/kottest-player:0.0.1
```

* Run image as given below:

```bash
~$ docker build -t franvallano/kottest-player:0.0.1 .
```
