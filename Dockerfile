#Imagen base - SSOO limpio que solo tiene JDK 8
FROM openjdk:8-jdk-alpine

COPY target/kottest-player-0.0.1-SNAPSHOT.jar /opt/

RUN groupadd -g 999 appgroup && \
    useradd -r -u 999 -g appgroup appuser && \
    chown -R appuser:appgroup /opt/

EXPOSE 8080

USER appuser
WORKDIR /opt/

ENTRYPOINT java -jar kottest-player-0.0.1-SNAPSHOT.jar
